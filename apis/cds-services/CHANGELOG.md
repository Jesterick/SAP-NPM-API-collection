# Changelog

All notable changes to this project will be documented in this file.

This project adheres to [Semantic Versioning](http://semver.org/).

The format is based on [Keep a Changelog](http://keepachangelog.com/).

## Version 0.11.0 - 2018-10-04

### Added

- Generic support for Create, Update, Delete on draft-enabled entities
- Generic support for draftEdit, draftPrepare, draftActivate actions
- Logger is available in handlers via context.log

### Changed

- Log warning if database connection is missing

### Fixed

- Service requests now return promises instead of thenables

## Version 0.10.1 - 2018-09-18

### Added

- Generic support for Read on draft-enabled entities

### Fixed

- $user annotation works without authorization

## Version 0.10.0 - 2018-09-17

### Added

- Delete Draft
- Audit Logging of GDPR related events
- Auto lookup of to be used CF/XSA services from environmental VCAP_SERVICES
- OData to context.query for nested $filter, $orderby, $op and $skip at $expand
- Custom types on top of associations

### Changed

- Default for maxPageSize increased to 1000 from 100

### Fixed

- Values for annotated columns (user/now) are included in the response

## Version 0.9.2 - 2018-09-05

### Changed

- Improved npm-shrinkwrap

## Version 0.9.1 - 2018-09-03

### Added

- Create draft

### Removed

- implicit dependency to @sap/cds-sql

## Version 0.9.0 - 2018-08-28

### Added

- API to support the implementation of authorization restrictions 
- Local service client
- Support for to-one-navigation in $filter
- Support for annotation @Search.defaultSearchElement to restrict searchable columns in $search
- Support for sap-language query parameter
- Support authorization annotations
- Hooks to add custom logic before and after rollback event
- Audit Logging of security events

### Fixed

- Pagination in case of $expand
- $select with managed associations as key

## Version 0.8.1 - 2018-08-09

### Added

- Authentication using passport (including user/attr proxy)

### Changed

- Require submodules on demand

## Version 0.8.0 - 2018-08-07

### Added

- OData Service: $search supports Unary and Binary Expressions without brackets
- Registration of global handler using star symbol like "this.on('*', () => {})"   
- Registration of express middleware using this.use()
- Improved FeautureNotSupported error message
- context.reject supported for before, on and after handlers 
- Support of context.run().then.run() shortcut

### Changed

- Updated version of @sap/odata-v4 to ^1.6.0

### Fixed

- Localization in case language is changed
- Issue with not working $count when filtering active in custom hook

## Version 0.7.0 - 2018-07-11

### Added

- Localization support for $metadata
- Support for Compositions

### Fixed

- $search also considers foreign keys of managed associations, structured elements and complex types

## Version 0.6.0 - 2018-07-02

### Added

- Multi tenancy support

### Fixed

- Columns are only added once to CQN in case of $expand in combination with $select

## Version 0.5.0 - 2018-06-25

### Added

- Hooks
  - An any handler can be registered and will be executed for any but COMMIT events
  - Custom handlers can be registered for before COMMIT and after COMMIT events
  - "_" property added to cds handler argument, which can contain adapter specific data like a request object

- OData Service
  - $filter supports (not) contains, startsWith, endsWith
  - $filter supports combinations with and/or
  - $select within $expand
  - $apply supported with limited scope
  - $search supported with limited scope

### Changed

- Hooks
  - Undocumented OData specific properties removed from "cds" handler argument
  - cds.target contains the unreflected entity instead of the reflected entity
  - cds.error will collect errors and throw at the end of each block of .before, .on or .after handlers
  - Second call to next() at a on handler will be ignored and not break the sequence

### Fixed

- Support navigation over entities with multiple keys

### Removed

- In case of a SerializationError the details are only logged and not provided in the response anymore

## Version 0.4.1 - 2018-05-03

### Changed

- Updated version of @sap/cds-ql to 0.4.1

## Version 0.4.0 - 2018-05-02

### Added

- service factory
  - cds used via injection

- Hooks
  - Support annotations @insertonly and @readonly
  - Support reject registration for CSN entities
  - Support reject registration with multiple entity parameters

### Changed

- default logger uses matching methods from console object instead of console.log
- packages are loaded on demand at Services.js and OData.js instead of required in any case
- adapted error message in case of 501

## Version 0.3.0 - 2018-04-16
### Added

- service factory
  - service.entities is set

- OData Service
  - Support for $expand=*
  - Support for $select=*

- Hooks
  - CSN entities can be used instead of strings to register a handler
  - .on can be registered with CQN instead of function as handler
  - .on supports registering N handlers
  - .on handlers can use a second argument next()
  - .on can be finished by returning a value
  - .after with convenience wrappers for each|row argument
  - .after can now work asynchronously


### Changed

- server side paging is enabled by default and set to 100, to disable it set maxPageSize to false.

- refactored service factory
  - removed option to compile CSN on the fly, only CSN accepted as input format
  - option to set the URL path is removed
  - Multi service CSN can be used

- refactored Service class
  - OData service instantiation is now split in constructor, createODataService and getMiddleWare

- OData Service
  - Renamed parameters in handler context object (target replaces entity and getEntity)
  - More expressive error messages
  - Crash Node.js instance on unhandled error

### Fixed

- limit property is only added to CQN if necessary
- .reply() is able to handle null values

## Version 0.2.0 - 2018-03-16
### Added

- option to enable debug mode for odata-v4
- default logger with option to register custom logger
- support for server side paging
- support for cds.serve, which is a Fluent API-style method to read service definitions from the given model(s) and construct services
- usage of npm-shrinkwrap

### Fixed

- $filter in combination with to many association
