# Changelog

All notable changes to this project will be documented in this file.

This project adheres to [Semantic Versioning](http://semver.org/).

The format is based on [Keep a Changelog](http://keepachangelog.com/).

## Version 0.9.0 - 2018-10-04

### Changed

- Updated version of @sap/cds-sql to 0.11.0

## Version 0.8.0 - 2018-09-17

### Fixed

- CQN queries with contains and expand (limitation: expanded columns cannot be part of contains)

## Version 0.7.1 - 2018-09-05
   
### Changed

- Improved npm-shrinkwrap

## Version 0.7.0 - 2018-08-28

### Changed

- API documentation updated

## Version 0.6.1 - 2018-08-09

### Changed

- Require submodules on demand

## Version 0.6.0 - 2018-08-07

### Added

- cds.Timestamp and cds.DateTime converted into ISO time format when reading
- Support for abstract placeholders #now and #user

### Fixed

- SQL error hides internal error messages and provides details in log

## Version 0.5.0 - 2018-06-25

### Added

- support execution of blocks of statements
- support plain mode of SQL name mapping
 
### Changed

 - Added SQL Error to hide the internal information from other errors

### Fixed

- CDS injection

## Version 0.4.0 - 2018-05-02

### Changed

- connect options aligned to spec
- support for latest CQN spec changes

## Version 0.3.0 - 2018-04-16

### Added

- support CREATE statements

## Version 0.2.0 - 2018-03-16

### Added

- usage of npm-shrinkwrap

### Changed

- improved performance
