# Change Log

All notable changes to this project will be documented in this file.

This project adheres to [Semantic Versioning](http://semver.org/).

The format is based on [Keep a Changelog](http://keepachangelog.com/).

## Unreleased

### Added
- amqp-v100 protocol support

### Changed
- adapt to new enterprise-messaging VCAP structure

### Removed