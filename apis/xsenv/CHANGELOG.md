# Change Log
All notable changes to this project will be documented in this file.

This project adheres to [Semantic Versioning](http://semver.org/).

The format is based on [Keep a Changelog](http://keepachangelog.com/).

## 1.2.9 - 2018-01-18
### Added
- Release with npm-shrinkwrap.json


## 1.2.8 - 2017-10-09
### Security
- Updated debug package to fix a security issue https://snyk.io/vuln/npm:debug:20170905
