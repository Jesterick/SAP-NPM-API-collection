# Change Log

All notable changes to this project will be documented in this file.

This project adheres to [Semantic Versioning](http://semver.org/).

The format is based on [Keep a Changelog](http://keepachangelog.com/).

## Version 1.7.0
### Changes
- Clean up of TypeScript APIs

## Version 1.6.0
### Features
- Getters for entities and services
- Infer keys for managed associations
- Support for getting entities and services with namespace, e.g. `cds.reflect(m).entities ('my.bookshop')`

## Version 1.5.0
### Fixes
- Fixes for linking associations and foreign keys
