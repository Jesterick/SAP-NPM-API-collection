## 1.4.2

Features:
- export the internal HTTP server
- use hdi-deploy version 3.8.2

## 1.2.2

Fixes:
- switch from res.send back to res.end to fix problems with the content type

## 1.2.1

Features:
- update dependencies

## 1.2.0

Features:
- additional route /v1/deploy/to/instance accepting the response of a instance manager GET
- use hdi-deploy version 3.4.0

## 1.1.0

Features:
- use hdi-deploy version 3.3.0
- move forking of hdi-deploy into hdi-deploy itself

## 1.0.0

initial release
