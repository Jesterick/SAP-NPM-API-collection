# Change Log
All notable changes to this project will be documented in this file.

This project adheres to [Semantic Versioning](http://semver.org/).

The format is based on [Keep a Changelog](http://keepachangelog.com/).

## 1.3.0 - 2018-01-19

### Added
- npm-shrinkwrap.json

## 1.2.0 - 2018-01-08

### Added
- CHANGELOG.md
- Node.js version 8 support

## 1.1.3 - 2017-07-04

### Removed
- Remove lodash as a productive dependency.

## 1.1.2 - 2017-01-24

### Changed
- Rename package to use *sap* scope.
