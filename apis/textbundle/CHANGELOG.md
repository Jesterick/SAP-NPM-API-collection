# Change Log
All notable changes to this project will be documented in this file.

This project adheres to [Semantic Versioning](http://semver.org/).

The format is based on [Keep a Changelog](http://keepachangelog.com/).

## 2.2.1 - 2018-02-07

### Fixed
- Missing typings from package

## 2.2.0 - 2018-01-09

### Added
- Node.js version 8 support
- CHANGELOG.md
- Typings

## 2.1.0 - 2017-07-04

### Added
- Static function TextBundle.fallbackLocale.

## 2.0.6 - 2017-01-25

### Changed
- Rename package to use @sap scope.
